<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\RevisorController;
use App\Http\Controllers\AnnouncementController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Public routes
Route::get('/', [PublicController::class, 'welcome'])->name('welcome');
Route::get('/index/{id}', [PublicController::class, 'index'])->name('category.index');


// Announcement route
Route::get('/announcement/index', [AnnouncementController::class, 'index'])->name('announcement.index');
Route::get('/announcement/create', [AnnouncementController::class, 'create'])->name('announcement.create');
Route::get('/announcement/show/{announcement}', [AnnouncementController::class, 'show'])->name('announcement.show');

// Become revisor routes
Route::get('/richiesta/revisore', [RevisorController::class, 'becomeRevisorView'])->name('become.revisor.view');
Route::get('/rendi/revisore/{user}', [RevisorController::class, 'makeRevisor'])->name('make.revisor');
Route::get('/richiesta/revisore/send', [RevisorController::class, 'becomeRevisorSend'])->name('become.request');

// Delete revisor routes
Route::get('/richiesta/cancella/revisore', [RevisorController::class, 'deleteRevisorView'])->name('delete.revisor.view');
Route::get('/cancella/revisore/{user}', [RevisorController::class, 'deleteRevisor'])->name('delete.revisor');
Route::get('/richiesta/cancella/revisore/send', [RevisorController::class, 'deleteRevisorSend'])->name('delete.revisor.request');

// Accept/decline announcement routes
Route::get('/revisor/index',[RevisorController::class, 'index'])->name('revisor.index');
Route::patch('/accetta/annuncio/{announcement}',[RevisorController::class, 'acceptAnnouncement'])->name('revisor.accept_announcement');
Route::patch('/rifiuta/annuncio/{announcement}',[RevisorController::class, 'rejectAnnouncement'])->name('revisor.reject_announcement');

//rotta undo, parametrica che accetta l'id che viene flashato come session key quando viene accettato o rifiutato un articolo
Route::get('/revisor/index_undo_{id}',[RevisorController::class,'undoAnnouncement'])->name('revisor.undo');

//ricerca annuncio
Route::get('/ricerca/annuncio',[PublicController::class, 'searchAnnouncement'])->name('announcement.search');

//cambia lingua
Route::post('/lingua/{lang}', [PublicController::class, 'setLanguage'])->name('setLocale');