let writer = document.querySelector('#writer');
let btnCategory = document.querySelectorAll('#btnCategory');
let catContain = document.querySelector('#catContain');

//verifica se stiamo su mobile
let mobile = true;
if(window.innerWidth>window.innerHeight) mobile=false;

//codice per fare la scritta dinamica dell'header
if(writer){
  let ch = writer.innerHTML.split("");
  let i = 0;
  writer.innerHTML = '';


  let typingInterval = setInterval(()=>{
    
    writer.innerHTML += ch[i];
    i++;
    if(i==ch.length){
      clearInterval(typingInterval);
    } 
  }, 100)

}

//codice per fare l'animazione del colore sul bottone dell'header
let btn = document.querySelector('.btn-main')

if(btn){
  
  btn.onmousemove = function(e) {
    let x = e.pageX - btn.offsetLeft - btn.offsetParent.offsetLeft
    let y = e.pageY - btn.offsetTop - btn.offsetParent.offsetTop
    btn.style.setProperty('--x', x + 'px')
    btn.style.setProperty('--y', y + 'px')
  }
}


//codice per fare partire le animazioni animEntrata, animEntrataLeft e animEntrataRight degli elementi
let btt = document.querySelector('#btt');
let anim_element_left = document.querySelectorAll('.fadeInLeft');
let anim_element_right = document.querySelectorAll('.fadeInRight');


if(mobile){

  btnCategory.forEach((element)=>{
    element.classList.remove('animEntrata');
  })

  anim_element_left.forEach(function(element){
      element.classList.remove('opacity-0');
      element.classList.add('animEntrataLeft');
  })

  anim_element_right.forEach(function(element){
      setTimeout(function(){
          element.classList.remove('opacity-0');
          element.classList.add('animEntrataRight');
      }, 200)
  })
}else{
  setTimeout(()=>{
    btnCategory.forEach((element)=>{
      element.classList.remove('animEntrata');
    })
  },1000)
}

window.addEventListener('scroll', ()=>{
  
  if(scrollY>90){

    if(!mobile){
      
      anim_element_left.forEach(function(element){
        element.classList.remove('opacity-0');
        element.classList.add('animEntrataLeft');
      })
      
      anim_element_right.forEach(function(element){
        setTimeout(function(){
          element.classList.remove('opacity-0');
          element.classList.add('animEntrataRight');
        }, 200)
      })
    } 
    
    btt.classList.remove('d-none');
    btt.classList.remove('animEntrataOut');
    btt.classList.add('animEntrata');
  } else{ 
   
    btt.classList.remove('animEntrata');
    btt.classList.add('animEntrataOut');
  }

})

