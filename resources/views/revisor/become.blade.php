<x-layout>

    <div class="container my-5">
        <div class="row justify-content-center">
            <div class="col-12">
                @livewire('become-revisor-form')
            </div>
        </div>
    </div>

</x-layout>