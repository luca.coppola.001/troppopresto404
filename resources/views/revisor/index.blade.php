<x-layout>
<div class="container my-5">
  <div class="row justify-content-center">
    @if(session()->has('announcementAccepted'))
      <div class="alert alert-success text-center">
          {{session('announcementAccepted')}}
      </div>
    @endif
    @if(session()->has('announcementRejected'))
      <div class="alert alert-danger text-center">
          {{session('announcementRejected')}}
      </div>
    @endif
    {{-- se la sessione ha la key undo, passate da accept e reject dentro al RevisorController, mostra il tasto per tornare indietro vicino al feedback della revisione --}}
    @if(session()->has('undoId'))
      <div class="col-12 d-flex justify-content-center">
        <form action="{{route('revisor.undo',['id'=>session('undoId')])}}">
          @csrf
          <button class="btn btn-warning rounded-pill" type="submit">{{__('ui.annullaBtnRev')}}</button>
        </form>
      </div>
    @endif
      <div class="col-12 text-center">
          <h1 class="display-2">{{$announcement_to_check ? __('ui.messRevDaRevisionare') : __('ui.messRevNonRev')}}
          </h1>
      </div>
  </div>
</div>

@if($announcement_to_check)

<div class="container">
  <div class="row justify-content-center">
    
    <div class="col-12 col-md-6">
      <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="true">

        {{-- abbiamo levato gli indicatori perché non contavano fino al numero di immagini  --}}
        {{-- <div class="carousel-indicators">
          <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
          <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
          <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
        </div> --}}

        <div class="carousel-inner">

          {{-- qui ci vanno le singole immagini del carosello --}}
          @foreach($announcement_to_check->images()->get() as $image)
          <div class="carousel-item @if($loop->first)active @endif">
            <div class="d-flex justify-content-center mx-1">
              <img src="{{$image->getUrl(400,300)}}" class="d-block w-100" alt="...">
              <div class="px-2"></div>
              <div class="col-md-3 ">
                  <div class="card-body">
                  <h5 class="tc-accent ">Revisione Immagini</h5>
                  <p>Adulti:<span class="{{$image->adult}}"></span></p>
                  <p>Satira:<span class="{{$image->spoof}}"></span></p>
                  <p>Medicina:<span class="{{$image->medical}}"></span></p>
                  <p>Violenza:<span class="{{$image->violence}}"></span></p>
                  <p>Contenuti Ammiccanti:<span class="{{$image->racy}}"></span></p>
                  </div>
              </div>
            </div>
          </div>
          
          
          
          @endforeach
        </div>

        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Previous</span>
        </button>

        <button class="carousel-control-next marginNext" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Next</span>
        </button>

      </div>
    </div>
              
    <div class="col-12 text-center">
      <h5>{{__('ui.nome-articolo')}} {{$announcement_to_check->title}}</h5>
      <p>{{__('ui.desc')}} {{$announcement_to_check->description}}</p>
      <p>{{__('ui.prezzo')}} {{$announcement_to_check->price}} &euro;</p>
    </div>
              
    <div class="row">
      
      <div class="col-6 col-md-6 text-center">
        <form action="{{route('revisor.accept_announcement',['announcement'=>$announcement_to_check])}}" method="POST">
          @csrf
          @method('PATCH')
          <button class="btn btn-success rounded-pill" type="submit">{{__('ui.accAnnBtn')}}</button>
        </form>
      </div>

      <div class="col-6 col-md-6 text-center">
        <form action="{{route('revisor.reject_announcement',['announcement'=>$announcement_to_check])}}" method="POST">
          @csrf
          @method('PATCH')
          <button class="btn btn-danger rounded-pill" type="submit">{{__('ui.noAnnBtn')}}</button>
        </form>
      </div>
    </div>
  </div>
</div>
@endif







</x-layout>