<div>
    <x-header>
        <h1 class="text-center">{{__('ui.insert')}}</h1>
    </x-header>

    {{-- messaggio di avvenuto successo o errore --}}
    @if(session('announcementCreated'))
        <div class="alert alert-success text-center">{{session('announcementCreated')}}</div>
    @elseif(session('announcementCreatedError'))
    <div class="alert alert-danger text-center">{{session('announcementCreatedError')}}</div>
    @endif

    <div class="container p-5 my-5 shadow border-purple bg-light rounded-5">
        <div class="row justify-content-center">
            <div class="col-12">

                <form wire:submit.prevent="store" enctype="multipart/form-data">
                    @csrf
                    
                    <div class="mb-3">
                        <label for="title" class="form-label">{{__('ui.nome-articolo')}}</label>
                        <input wire:model.debounce.1000="title" type="text" @error ('title') is-invalid @enderror class="form-control" id="title">
                        {{-- errore validazione title --}}
                        @error('title') <span class="error text-danger small">{{ $message }}</span> @enderror
                    </div>
                    <br>

                    
                    <label for="category_id" class="form-label">{{__('ui.cat')}}</label>
                    <select wire:model="category_id" name="" id="category_id">
                        <option class="text-category" value="">{{__('ui.allcat')}}</option>
                        @foreach($categories as $category)
                            <option value="{{$category->id}}">{{$category->name}}</option>              
                        @endforeach
                    </select>
                    {{-- errore validazione category --}}
                    <div class="mb-3">
                        @error('category_id') <span class="error text-danger small">{{ $message }}</span> @enderror
                    </div> 
                    
                    <div>
                        <div class="mb-3">
                            <label for="price" class="form-label">{{__('ui.prezzo')}}</label>
                            <input wire:model.debounce.1000="price" type="text"  @error ('price') is-invalid @enderror class="form-control" id="price">
                            {{-- errore validazione price --}}
                            @error('price') <span class="error text-danger small">{{ $message }}</span> @enderror
                        </div>
                    </div>

                    <div>
                        <div class="mb-3">
                            <label for="description" class="form-label">{{__('ui.dett')}}</label>
                            <textarea wire:model.debounce.1000="description" type="text"  @error ('description') is-invalid @enderror class="form-control" id="description"></textarea>
                            {{-- errore validazione descrition --}}
                            @error('description') <span class="error text-danger small">{{ $message }}</span> @enderror
                        </div>
                    </div>
                    
                    <!-- campo inserimento immagini -->
                    <div class="mb-3">
                        <input wire:model="temporary_images" multiple class="form-control shadow @error('temporary_images.*') is_invalid @enderror" placeholder="inserisci immagine" name="images" type="file">
                        @error('temporary_images.*') <span class="text-danger mt-2">{{$message}}</span> @enderror
                    </div>
                    @if(!empty($images))
                        <div class="row">
                            <div class="col-12">
                                <p>Anteprima</p>
                                <div class="row border border-4 py-4">
                                    @foreach($images as $key=>$image)
                                        <div class="col my-3">
                                            <div class="previewSize mx-auto shadow rounded" style="background-image: url('{{$image->temporaryUrl()}}');">
                                            </div>
                                            <button type="button" class="btn btn-danger shadow d-block mt-2 mx-auto" wire:click="removeImage({{$key}})">{{__('ui.eliminaPrevFoto')}}</button>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @endif

                    <div class="mb-3">
                        <button type="submit" class="btn btn-purple">{{__('ui.inviaAnn')}}</button>
                        <a class="purple" href="{{route('welcome')}}">{{__('ui.returnHome')}}</a>
                    </div>
                    
                </form>

            </div>
        </div>
    </div>

</div>

