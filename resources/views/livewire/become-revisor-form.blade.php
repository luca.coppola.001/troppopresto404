<div>

    <div class="container p-5 shadow my-5 bg-light rounded-5 border-purple">
        <div class="row justify-content-center">
            <div class="col-12">

                <form>
                    @csrf
                    <div class="mb-3">
                        <label for="email" class="form-label">Email</label>
                        <input name="email" type="email"  class="form-control" id="email" value="{{Auth::user()->email}}" disabled>
                    </div>
                    <div class="mb-3">
                        <label for="name" class="form-label">Username</label>
                        <input name="name" type="text"  class="form-control" id="name" value="{{Auth::user()->name}}" disabled>
                    </div>
                    <p class="text-center">{{__('ui.inviaRicRev')}}</p>
                    </div>
                    <a href="{{route('become.request')}}" class="btn btn-purple px-2">{{__('ui.inviaRev')}}</a>
                    <a href="{{route('welcome')}}" class="purple ms-2">{{__('ui.returnHome')}}</a>
                </form>
                
            </div>
        </div>
    </div>

</div>
    

