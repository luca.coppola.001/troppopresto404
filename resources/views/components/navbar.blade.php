<nav class="shadow position-sticky navZindex top-0">

  <div class="container-fluid  cover border-down bg-light">   
    <div class="row text-center ">
      
      <div class="col-md-3 col-12 py-md-4 py-1 d-flex justify-content-md-around justify-content-center">
        <img class="ms-3" src="{{Storage::url('public/root_media/flamingo.png')}}" height="50px" alt="non trovato">
        <a class="navbar-brand title-purple mt-2" href="{{route('welcome')}}">Troppo presto</a>
        
        
        <div class="dropdown">
          <a class="btn dropdown-toggle ms-1" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            <x-_locale Lang="{{session()->get('locale') ? session()->get('locale') : 'it'}}" />
          </a>
        
          <ul class="dropdown-menu dropdown-menu-locale px-0 mx-0">
            <li class="d-flex justify-content-center"><x-_locale Lang="it" /></li>
            <li class="d-flex justify-content-center"><x-_locale Lang="en" /></li>
            <li class="d-flex justify-content-center"><x-_locale Lang="ro" /></li>
          </ul>
        </div>
        
      </div>

      <div class="@auth col-md-5 @endauth @guest col-md-7 @endguest col-12 py-md-4 py-1 pe-md-0 pe-4 mt-1 ">

        <form role="search" action="{{route('announcement.search')}}" method="GET" class="d-flex"> 

          <div class="input-group ">
                    {{-- bottone cerca --}}
            <input name="searched" class="form-control rounded-searchbar" type="search" placeholder="Search" aria-label="Search">
            <button class="input-group-text btn btn-purple rounded-bttsearch px-md-5" type="submit">{{__('ui.search')}}</button>

              {{-- sezione utente auth --}}
              @auth
                @if (Auth::user()->is_revisor)
                  <div class="nav-item ms-4">
                      <a href="{{route('revisor.index')}}" class="btn btn-purple">{{__('ui.rev')}}</a>
                      <span class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger">
                        {{App\Models\Announcement::toBeRevisionedCount()}}
                        <span class="visually-hidden">unread messages</span>
                      </span>
                  </div>
                @endif
              @endauth
          </div> 

        </form>

      </div>

      <div class=" @auth col-md-4 @endauth @guest col-md-2 @endguest col-12 py-md-4 py-1 ps-0  mt-1">
        @guest
          <a href="{{route('login')}}" class="btn btn-outline-purple">{{__('ui.regLog')}}</a>
        @endguest
      {{-- benvenuto --}}
        @auth
        <div class="d-flex justify-content-around">

          {{-- inserisci annuncio --}}
          <a href="{{route('announcement.create')}}" class="nav-link ms-2 px-2 btn btn-purple">{{__('ui.insert')}}</a>

          {{-- benvenuto utente --}}
          <h5 class="nav-item my-1">{{__('ui.benvenuto')}} {{Auth::user()->name}} </h5>

          
          {{-- logout --}}
          <a class="nav-link p-1 px-2 btn btn-outline-purple" aria-current="page" href="#" onclick="event.preventDefault(); document.querySelector('#form-logout').submit();"> <span class="pt-1">Logout</span>
          </a>
          <form action="{{route('logout')}}" id="form-logout" method="POST" class="d-none">
            @csrf
          </form>
        </div>
        @endauth
          </div>  
        </div>
      
  </div>

  {{-- inizio categorie --}}
  <div class="navbar container-fluid bg-light mx-0 justify-content-center" id="catContain">

    <div class="collapse {{-- Route::is('welcome') ? 'show' : '' --}} justify-content-center w-100" id="catToggle">
      <div class="row justify-content-around text-center">
        @foreach($categories as $category)
          <div class="col-4 col-md-2 my-2 mx-2 {{Route::is('welcome') ? 'animEntrata' : ''}}" id="btnCategory"> <a href="{{route('category.index',$category->id)}}" class="btn btn-category w-100"><i class="fas me-1 category-{{$category->id}}"></i>{{$category->name}}</a> </div>
        @endforeach
        {{-- bottone tutti gli annunci --}}
        <div class="col-4 col-md-2 my-2 {{Route::is('welcome') ? '' : ''}}" id="btnCategory"> <a href="{{route('announcement.index')}}" class="btn btn-purple w-100 ">{{__('ui.annAllBtn')}}</a> </div>
      </div>
    </div>

    {{-- bottone categorie --}}
    <button class="navbar-toggler btn btn-category" type="button" data-bs-toggle="collapse" data-bs-target="#catToggle" aria-controls="catToggle" aria-expanded="false" aria-label="Toggle navigation">
      <div class=" p-1">{{__('ui.allCatBtn')}}</div>
    </button>

  </div>
  

  
</nav>