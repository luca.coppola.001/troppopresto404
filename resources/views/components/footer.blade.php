<!-- <div class="container-fluid p-5 bg-dark mt-2">
    <div class="row justify-content-center">
        <div class="col-12 d-flex justify-content-center">
            
                <img src="{{Storage::url('public/root_media/flamingo-white.png')}}" class="sizeFlamingoFooter" alt="">
            
        
        @guest
        <a href="{{route('become.revisor.view')}}" class="btn btn-warning text-dark my-3">{{__('ui.revisorButt')}}</a>
        @endguest
        @auth
        @if(!Auth::user()->is_revisor)
            <a href="{{route('become.revisor.view')}}" class="btn btn-warning text-dark my-3">{{__('ui.revisorButt')}}</a>
        @else
        <div class="d-flex flex-column">
            <a href="{{route('revisor.index')}}" class="btn btn-warning text-dark my-3">{{__('ui.revAnnFooter')}}</a>
            <hr class="border-light">
            <a href="{{route('delete.revisor.view')}}" class="btn btn-warning text-dark my-3">{{__('ui.nonPiuREv')}}</a>
        </div>
        @endif
        @endauth
        </div>
    </div>
</div> -->


<div class="container-fluid p-5 bg-light Fshadow mt-2">
    <div class="row justify-content-center">

        
        <div class="col-12 d-flex justify-content-center">
        
            @guest
                <a href="{{route('become.revisor.view')}}" class="btn btn-purple text-white ms-md-2 my-3">{{__('ui.revisorButt')}}</a>
            @endguest

            @auth
                @if(!Auth::user()->is_revisor)
                    <a href="{{route('become.revisor.view')}}" class="btn btn-purple text-white my-3">{{__('ui.revisorButt')}}</a>
                @else
                    <div class="d-flex flex-column">
                        <a href="{{route('revisor.index')}}" class="btn btn-purple text-white my-3">{{__('ui.revAnnFooter')}}</a>
                        <hr class="border-dark">
                        <a href="{{route('delete.revisor.view')}}" class="btn btn-purple text-white my-3 mb-md-4">{{__('ui.nonPiuREv')}}</a>
                    </div>
                @endif
            @endauth
            
        </div>

        <div class="container">
            <div class="row">
                <div class="col-12 d-flex justify-content-center">
                    <img src="{{Storage::url('public/root_media/flamingo_white.png')}}" class=" mt-md-2 imgPNGborder sizeFlamingoFooter" alt="">
                    <ul class="m-0 d-flex flex-column justify-content-center p-0 text-center">
                        <h4>Team Staff:</h4>
                        <hr>
                        <li class=""><a target="_blank" class="footerlinks link-dark" href="//www.linkedin.com/in/beniamin-chira-web-developer">Beniamin Chira</a></li>
                        <li class=""><a target="_blank" class="footerlinks link-dark" href="//www.linkedin.com/in/luca-coppola-full-stack-developer/" class="link-dark">Luca Coppola</a></li>
                        <li class=""><a target="_blank" class="footerlinks link-dark" href="//www.linkedin.com/in/sergioprocentese/" class="link-dark">Sergio Procentese</a></li>
                        <li class=""><a target="_blank" class="footerlinks link-dark" href="//www.linkedin.com/in/simone-de-meis-webdeveloper/" class="link-dark">Simone De Meis</a></li>
                        <li class=""><a target="_blank" class="footerlinks link-dark" href="//www.linkedin.com/in/stefano-muscettola-b4535a207" class="link-dark">Stefano Muscettola</a></li>
                    </ul>
                </div>
            </div>
        </div>
            

    </div>
</div>

   