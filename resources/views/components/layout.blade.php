<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Presto</title>
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Arvo:ital,wght@0,400;0,700;1,400;1,700&family=Bitter:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Corben:wght@400;700&display=swap" rel="stylesheet"> 
        
        {{-- script icona share --}}
        <script type="text/javascript" src="https://platform-api.sharethis.com/js/sharethis.js#property=63826865233b3c001362c71e&product=inline-share-buttons&source=platform" async="async"></script>
    
        {{-- icone bootstrap --}}
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.2/font/bootstrap-icons.css">
        {{-- Icone Fontawesome --}}
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/all.css" integrity="sha384-DyZ88mC6Up2uqS4h/KRgHuoeGwBcD4Ng9SiP4dIRy0EXTlnuz47vAwmeGwVChigm" crossorigin="anonymous"/>
        {{-- vite --}}
        @vite(['resources/css/app.css', 'resources/js/app.js'])
        

        @livewireStyles

    </head>
    <body>

        <x-navbar />

        <x-header />

        <div class="min-vh-100">
            {{$slot}}
        </div>

        <a href="#" class="btn btn-purple position-fixed p-3 rounded-circle d-none" id="btt">{{__('ui.top')}}</a>

        <x-footer />

        @livewireScripts

    </body>
</html>