<x-layout>

    <div class="container z-10">
        <div class="row justify-content-center animEntrataLeft">
           
            @forelse ($announcements as $announcement)
                <div class="col-12 col-md-4 my-3">
                    <div class="card cardZindex">
                        <img src="{{!$announcement->images()->get()->isEmpty() ? $announcement->images()->first()->getUrl(400,300) : 'https://picsum.photos/400/300'}}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <p class="card-text fs-4 fw-bold">{{$announcement->price}}&euro;</p>
                            <h5 class="card-title text-truncate">{{$announcement->title}}</h5>
                            <p class="card-text text-truncate">{{$announcement->description}}</p>
                            <p class="card-text text-truncate">{{__('ui.cat')}}: {{$announcement->category->name}}</p>
                            <p class="small ">{{__('ui.inseritoIl')}} {{$announcement->created_at->format('d/M/Y H:i')}}</p> 
                            <a href="{{route('announcement.show', $announcement)}}" class="btn btn-purple ">Vai all'annuncio</a>
                        </div>
                    </div>
                </div>
            @empty
                <h1 class="d-flex justify-content-center">{{__('ui.noAnn')}}</h1>
            @endforelse
            
        </div>
    </div>

</x-layout>

