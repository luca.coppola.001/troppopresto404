<x-layout>

  <div class="text-center my-5">
    <h3>Registrati</h3>
  </div>

  <div class="container p-5 shadow my-5 bg-light rounded-5 border-purple">
    <div class="row justify-content-center">
      <div class="col-12">

        @if ($errors->any())
          <div class="alert alert-danger">
            <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
        @endif

        <form method="POST" action="{{route('register')}}">
          @csrf

          <div class="mb-3">
            <label for="email" class="form-label">Email</label>
            <input name="email" type="email"  class="form-control" id="email">
          </div>

          <div class="mb-3">
            <label for="name" class="form-label">Username</label>
            <input name="name" type="text"  class="form-control" id="name">
          </div>

          <div class="mb-3">
            <label for="password" class="form-label">Password</label>
            <input name="password" type="password" class="form-control" id="password">
          </div>

          <div class="mb-3">
            <label for="password_confirmation" class="form-label">Conferma password</label>
            <input name="password_confirmation" type="password" class="form-control" id="password_confirmation">
          </div>

          <button type="submit" class="btn btn-purple px-2">{{__('ui.register')}}</button>
          <a href="{{route('login')}}" class="purple ms-2">{{__('ui.nonRegBtn')}}</a>
          
        </form>

      </div>
    </div>
  </div>

</x-layout>