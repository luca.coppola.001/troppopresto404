<x-layout>
  <div class="text-center my-5">
    <h3>Effettua il login</h3>
  </div>

  {{-- messaggio di errore --}}
  @if ($errors->any())
  <div class="d-flex justify-content-center">
    <div class="alert alert-danger w-50">
        <p>Ci sono Errori di validazione:</p>
        <ul>
            <li>Controlla la tua Email o Password;</li>
            
        </ul>
    </div>
</div>
@endif

  {{-- @if(session('password'))
    <div class="alert alert-danger">
        <p>{{session('password')}}</p>        
    </div>
  @endif --}}

  {{-- messaggio di errore --}}

  <!-- {{-- form login --}} -->
  <div class="container p-5 my-5 shadow bg-light rounded-5 border-purple">
    <div class="row justify-content-center">
      <div class="col-12">

      <form method="POST" action="{{route('login')}}">
        @csrf

        <div class="mb-3">
          <label for="email" class="form-label">Email</label>
          <input name="email" type="email"  class="form-control" id="email">
        </div>

        <div class="mb-3">
          <label for="password" class="form-label">Password</label>
          <input name="password" type="password" class="form-control" id="password">
        </div>

        <div class="mb-3 form-check">
          <input type="checkbox" class="form-check-input" id="remember">
          <label class="form-check-label" for="remember">{{__('ui.remember')}}</label>
        </div>

        <button type="submit" class="btn btn-purple px-4">Login</button>
        <a href="{{route('register')}}" class="purple ms-2">{{__('ui.nonReg')}}</a>

      </form>

      </div>
    </div>    
  </div>

</x-layout>