<x-layout>

  <div class="container animEntrata">
    <div class="row justify-content-center m-3">
      <div class="col-12 col-md-8">
        
        {{-- sezione superiore del carosello --}}
        <div class="bg-white border rounded-3 border-3 shadow">

          <div class="d-flex m-md-2 justify-content-between">
            <a href="{{route('announcement.index')}}" class=""><i class="bi bi-arrow-left fs-2 fs-md-3 mt-md-3"></i></a>
            <p class="mt-md-2 ms-md-0 ms-3"> {{__('ui.nomVenditore')}} <br> {{$announcement->user->name}}</p>
            <p class="mt-md-2 ms-3 me-md-5 text-center">{{__('ui.valutazione')}}<br>&#9733;&#9733;&#9733;&#9733;&#9733;</p>
            {{-- dropdown condividi --}}
           
             {{-- <div class="dropdown dropend">
              <button class=" mt-md-1 mt-2 me-md-0 me-2" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                <i class=" bi bi-share "></i>
                </button>
              <ul class="dropdown-menu">
                <li><a class="dropdown-item" href="#">Action</a></li>
                <li><a class="dropdown-item" href="#">Another action</a></li>
                <li><a class="dropdown-item" href="#">Something else here</a></li>
              </ul>
            </div>  --}}
            
            {{-- fine dropdown --}}
            
           <div class="sharethis-inline-share-buttons"></div>
          </div>
          {{-- fine sezione superiore --}}
            
          
          {{-- carosello immagini --}}
           
          <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-inner">

            <!-- Carosello da gestire -->
            {{-- @dd($announcement->images()->get()) --}}
              @foreach($announcement->images()->get() as $image) 
                <div class="carousel-item @if($loop->first) active @endif">
                  <img src="{{$image->getUrl(400,300)}}" class="d-block w-100" alt="...">
                </div>
              @endforeach
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="visually-hidden">Next</span>
            </button>
          </div>

            {{-- titolo, descrizione, prodotto ecc. --}}

          <div class="m-2">

            <p class="card-text fs-4 fw-bold">{{$announcement->price}} &euro;</p>
            <p>{{__('ui.nome-articolo')}} {{$announcement->title}}</p>
            <p>{{__('ui.desc')}} {{$announcement->description}}</p>
            <p>{{__('ui.cat')}}: {{$announcement->category->name}}</p>
            <p class="small ">{{__('ui.inseritoIl')}} {{$announcement->created_at->format('d/M/Y H:i')}}</p> 
            
            {{--parte inferiore  --}}
            <div class="d-flex justify-content-between">
              <a href="{{route('announcement.index')}}" class="btn btn-purple"><i class="bi bi-arrow-left mt-5"></i> {{__('ui.indAnn')}}</a>
              <a href="{{route('welcome')}}" class="purple mt-2 ms-2">{{__('ui.returnHome')}}</a>
              
              {{-- dropdown venditore --}}
            
               {{-- <div class="dropdown ms-2 mt-1">
              <button class="btn text-center me-md-0 me-2 btn-success dropdown-toggle" data-bs-auto-close="false" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                Contatta via email:
              </button>
              <ul class="dropdown-menu">
                <li class="m-1">{{$announcement->user->email}}</li> 
                </ul>
            </div>
          </div>  --}}
          
          {{-- fine dropdown --}}
          </div>
          {{-- fine parte inferiore --}}
        </div>

      </div>
    </div>
  </div>
            
    {{--chiusura card  --}}
  
</x-layout>



        

        
        
          
          
          


            {{-- <div class="card">
                <div class="card-body">
                  <h5 class="card-title">{{$announcement->title}}</h5>
                  <p class="card-text">{{$announcement->description}}</p>
                </div>
            </div> --}}



