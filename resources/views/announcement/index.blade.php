<x-layout>

    <div class="container z-10">
        <div class="row text-center animEntrataLeft my-5">
            <h2>{{__('ui.allAnnouncements')}} </h2>
        </div>
        <div class="row justify-content-center animEntrataRight">
            
            @forelse($announcements as $announcement)    
            {{-- singola card     --}}
            <div class="col-12 col-md-4 my-3">
                    <div class="card cardZindex shadow">
                    <img src="{{!$announcement->images()->get()->isEmpty() ? $announcement->images()->first()->getUrl(400,300) : 'https://picsum.photos/400/300'}}" class="card-img-top rounded-1" alt="...">
                        <div class="card-body">
                            <p class="card-text fs-4 fw-bold">{{$announcement->price}}&euro;</p> 
                            <h5 class="card-title text-truncate">{{$announcement->title}}</h5>
                            <p class="card-text text-truncate">{{$announcement->description}}</p>
                            <p class="card-text text-truncate">{{__('ui.cat')}}: {{$announcement->category->name}}</p>
                            <p class="small ">{{__('ui.inseritoIl')}} {{$announcement->created_at->format('d/M/Y H:i')}}</p>
                            <a href="{{route('announcement.show', $announcement)}}" class="btn btn-purple ">{{__('ui.dettAnn')}}</a>
                        </div>
                    </div>
                </div>
            @empty
                <div class="col-12">
                    <div class="alert alert-warning">
                        <p class="lead">Non ci sono annunci da visualizare!</p>
                    </div>                  
                </div>
            @endforelse

                {{-- {{$announcement->links()}} --}}
        </div>
    </div>


</x-layout>