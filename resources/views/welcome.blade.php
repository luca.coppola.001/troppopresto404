<x-layout>

    {{-- <div class="container my-5"> --}}
    {{-- <div class="row justify-content-center"> dove chiudono questi div? --}}
        
        
        {{-- <div class="row justify-content-center"> dove chiudono questi div? --}}
            
            {{-- <div class="container my-5"> --}}
                {{-- <div class="row justify-content-center"> dove chiudono questi div? --}}
    <div class="container-fluid bg-header">

        <div class="container py-5 p-md-5 animEntrataLeft ">
            <div class="p-md-5">
    
                {{-- sezione alert --}}
                @if(session()->has('revisorRequestSent'))
                    <div class="alert alert-success text-center">
                        {{session('revisorRequestSent')}}
                    </div>
                @endif
    
                @if(session()->has('revisorRequestAccepted'))
                    <div class="alert alert-success text-center">
                        {{session('revisorRequestAccepted')}}
                    </div>
                @endif
    
                @if(session()->has('revisorDeleteRequestAccepted'))
                    <div class="alert alert-danger text-center">
                        {{session('revisorDeleteRequestAccepted')}}
                    </div>
                @endif
    
                @if(session()->has('access.denied'))
                    <div class="alert alert-danger text-center">
                        {{session('access.denied')}}
                    </div>
                @endif
                {{-- chiusura sezione alert --}}
    
                <div class="shadow-header shadow p-2 pt-4 rounded-4">

                    <h1 class="text-center arvo px-5">
                        {{__('ui.welcome')}}<br> <span class="bold purple" id="writer">TROPPO PRESTO!</span> 
                    </h1>
        
                    <div class="d-flex justify-content-center my-4">
                        <a href="{{route('announcement.index')}}" class="btn-main px-5 text-white arvo fs-3 rounded-5">
                            <span>{{__('ui.ads')}}</span>
                        </a>
                    </div>
                </div>
    
            </div>
        </div>
    </div>

    <div class="container-fluid bg-white py-3 shadow text-center animEntrataRight">
        <h2>{{__('ui.last')}}</h2>
    </div>

    {{-- card Annunci --}}
    <div class="container z-10">
        <div class="row justify-content-center bg-white">

            @foreach($announcements as $announcement)    
                <div class="col-12 col-md-4 my-3">
                    <div class="card cardZindex">
                        <img src="{{!$announcement->images()->get()->isEmpty() ? $announcement->images()->first()->getUrl(400,300) : 'https://picsum.photos/400/300'}}" class="card-img-top rounded-1" alt="...">
                        <div class="card-body">
                            <p class="card-text fs-4 fw-bold">{{__('ui.prezzo')}}  {{$announcement->price}} &euro;</p>
                            <h5 class="card-title text-truncate">{{__('ui.nome-articolo')}} {{$announcement->title}}</h5>
                            <p class="card-text text-truncate">{{__('ui.desc')}}  {{$announcement->description}}</p>
                            <p class="card-text text-truncate">{{__('ui.cat')}}: {{$announcement->category->name}}</p>
                            <p class="small ">{{__('ui.inseritoIl')}} {{$announcement->created_at->format('d/M/Y H:i')}}</p>
                            <a href="{{route('announcement.show', $announcement)}}" class="btn btn-purple ">{{__('ui.dettAnn')}}</a>
                        </div>
                    </div>
                </div>
            @endforeach

        </div>
    </div>
   
        
   

</x-layout>