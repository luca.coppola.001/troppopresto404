<?php

namespace App\Http\Livewire;

use Livewire\Component;

class DeleteRevisorForm extends Component
{
    public function render()
    {
        return view('livewire.delete-revisor-form');
    }
}
