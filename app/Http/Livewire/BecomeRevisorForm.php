<?php

namespace App\Http\Livewire;

use Livewire\Component;

class BecomeRevisorForm extends Component
{
    public function render()
    {
        return view('livewire.become-revisor-form');
    }
}
