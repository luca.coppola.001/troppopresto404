<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Category;
use App\Jobs\RemoveFaces;
use App\Jobs\ResizeImage;
use App\Models\Announcement;

use Livewire\WithFileUploads;
use App\Jobs\GoogleVisionLabelImage;
use App\Jobs\GoogleVisionSafeSearch;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

class AnnouncementCreateForm extends Component
{
    use WithFileUploads;

    public $images = [];
    public $temporary_images;

    public $title, $price, $description, $category_id, $announcement;

    // regole di validazione
protected $rules=[
    'title'=> 'required|min:3',
    'price'=>'required',
    'description'=>'required',
    'category_id'=>'required',
    'images.*'=>'required|image',
    'temporary_images.*'=>'required|image',

];
// fine regole di validazione

// messaggi di errore custom
protected $messages = [
    'title.required'=> 'Questo campo è obbligatorio',
    'price.required'=>'Questo campo è obbligatorio',
    'description.required'=>'Questo campo è obbligatorio',
    'category_id.required'=>'Questo campo è obbligatorio',
    'images.required'=>'Questo campo è obbligatorio',
    'images.image'=>'Puoi caricare solo immagini',
    'temporary_images.required'=>'Questo campo è obbligatorio',
    'temporary_images.image'=>'Puoi caricare solo immagini',
    
];

public function updatedTemporaryImages(){
    if($this->validate(['temporary_images.*'=>'image'])){
        foreach($this->temporary_images as $image){
            $this->images[] = $image;
        }
    }
}

public function removeImage($key){
    if(in_array($key, array_keys($this->images))){
        unset($this->images[$key]);
    }
}

// real time validation
public function updated($propertyName)
{
    $this->validateOnly($propertyName);
}
// fine real time validation
    public function store(){
        
        $this->validate();
        
        
        
        
        // $announcement = Announcement::create([
            //     'title'=>$this->title,
            //     'price'=>$this->price,
            //     'description'=>$this->description,
            //     'user_id' =>Auth::user()->id,
            //     'category_id'=>$this->category_id,         
            // ]);
            
            $this->announcement = Category::find($this->category_id)->announcements()->create($this->validate());
            
            if(count($this->images)){
                foreach($this->images as $image){
                    //$this->announcement->images()->create(['path'=>$image->store('images' , 'public')]);
                    
                    $newFileName = "announcements/{$this->announcement->id}";
                    $newImage =  $this->announcement->images()->create(['path'=>$image->store($newFileName , 'public')]);
                    
                    RemoveFaces::withChain([

                        new ResizeImage($newImage->path , 400 , 300),
                        new GoogleVisionSafeSearch($newImage->id),
                        new GoogleVisionLabelImage($newImage->id)

                    ])->dispatch($newImage->id);



                }
                File::deleteDirectory(storage_path('/app/livewire-tmp'));
            }
            
            session()->flash('announcementCreated', 'Annuncio creato con successo, sarà pubblicato dopo la revisione!');
            session()->flash('announcementCreatedError', "Non è stato possibile pubblicare il tuo annuncio riprova più tardi");
            
            $this->announcement->user()->associate(Auth::user());
            $this->announcement->save();
        
            $this->reset();

    }

    public function render()
    {
        $categories = Category::all();
        return view('livewire.announcement-create-form', compact('categories'));
    }
}
