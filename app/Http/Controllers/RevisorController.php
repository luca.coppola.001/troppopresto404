<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Announcement;
use Illuminate\Http\Request;
use App\Mail\BecomeRevisorMail;
use App\Mail\DeleteRevisorMail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Artisan;

class RevisorController extends Controller
{

    public function __construct(){

        $this->middleware('auth');

        $this->middleware('is_revisor')->except('becomeRevisorView', 'becomeRevisorSend', 'makeRevisor');
    }

    public function index(){

        $announcement_to_check = Announcement::where('is_accepted' , null)->first();    

        return view ('revisor.index' , compact('announcement_to_check'));

    }

    public function acceptAnnouncement(Announcement $announcement){

        $announcement->setAccepted(true);

        session()->flash('undoId', $announcement->id);

        return redirect()->back()->with('announcementAccepted' , "Hai accettato l'annuncio");
    }

    public function rejectAnnouncement(Announcement $announcement){

        $announcement->setAccepted(false);

        session()->flash('undoId', $announcement->id);

        return redirect()->back()->with('announcementRejected' , "Hai rifiutato l'annuncio");
    }
    

    //funzione di undo, si prende l'id tramite la session key che viene flashata da accept e reject, e con quello riprende l'articolo.
    public function undoAnnouncement($id){

        $announcement_to_check = Announcement::where('id' , $id)->first();

        //L'articolo una volta preso con la query viene reso di nuovo NULL, così da essere di nuovo preso in considerazione da index()
        $announcement_to_check->setAccepted(NULL);

        return redirect()->back(); //redirect back porta indietro, l'articolo può essere rivisto
    }
    //fine funzione undo


    public function becomeRevisorView(){

        return view ('revisor.become');
    }

    public function becomeRevisorSend(){

        Mail::to('admin@troppopresto.it')->send(new BecomeRevisorMail(Auth::user()));

        return redirect('/')->with('revisorRequestSent', 'Hai inviato correttamente la richiesta');
    }

    public function makeRevisor(User $user){

        Artisan::call('presto:makeUserRevisor', ["email"=>$user->email]);
        
        return redirect('/')->with('revisorRequestAccepted', 'L\'utente è diventato revisore');
    }

    // Delete revisor
    public function deleteRevisorView(){

        return view ('revisor.delete');
    }

    public function deleteRevisorSend(){
        Mail::to('admin@troppopresto.it')->send(new DeleteRevisorMail(Auth::user()));
        return redirect('/')->with('revisorRequestSent', 'Hai inviato correttamente la richiesta');
        
    }

    public function deleteRevisor(User $user){

        Artisan::call('presto:deleteUserRevisor', ["email"=>$user->email]);
        
        return redirect('/')->with('revisorDeleteRequestAccepted', 'L\'utente non è più revisore');
    }
}
