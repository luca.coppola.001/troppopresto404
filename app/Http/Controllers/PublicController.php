<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Announcement;
use Illuminate\Http\Request;

class PublicController extends Controller
{
    public function welcome(){

       $announcements=Announcement::where('is_accepted', true)->OrderBy('created_at','DESC')->take(3)->get();

        return view('welcome', compact('announcements'));
    }

    public function index($id){

        foreach(Category::all() as $category){
            if($category->id == $id){
                $announcements = $category->announcements->where('is_accepted',true);

                if($announcements){

                    return view('category.index', compact('announcements'));
                    
                }else return view('category.index');
            }
        }
        
    }

    public function searchAnnouncement(Request $request){

        $announcements = Announcement::search($request->searched)->where('is_accepted',true)->paginate(10);

        return view('announcement.index', compact('announcements'));
    }

    //cambiare la lingua
    public function setLanguage($lang)
    {
        session()->put('locale', $lang);
        return redirect()->back();
    }
}
