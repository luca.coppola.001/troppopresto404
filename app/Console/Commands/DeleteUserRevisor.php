<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;

class DeleteUserRevisor extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'presto:deleteUserRevisor {email}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Revoca lo status di revisore di un utente';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $user = User::where('email', $this->argument('email'))->first();
        if(!$user){
            $this->error('utente non trovato');
        return;
        }
        $user->is_revisor = false;
        $user->save();
        $this->info("L'utente {$user->name} non è più un revisore");    }
}
